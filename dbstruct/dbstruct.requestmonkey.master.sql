# --------------------------------------------------------------------------- #


#
#   RequestMonkey
#   -------------
#   The database structure
#


REPLACE INTO db_version VALUES('requestmonkey','1');


# --------------------------------------------------------------------------- #


#
#  Requests
#

DROP TABLE IF EXISTS requestmonkey_request;
CREATE TABLE requestmonkey_request
(
  request_uuid                 VARCHAR(64) NOT NULL,           #  Request UUID
  request_type                 VARCHAR(255),                   #  Request type, if provided

  request_tstamp               DATETIME NOT NULL,              #  Request timestamp
  request_uri                  TEXT NOT NULL,                  #  URI
  request_method               VARCHAR(32) NOT NULL,           #  Request method

  request_remote_ip            VARCHAR(255) NOT NULL,          #  Remote IP
  request_remote_host          VARCHAR(255),                   #  Remote host, if resolved

  request_request_header       MEDIUMTEXT NOT NULL,            #  Request headers
  request_request_body         MEDIUMTEXT NOT NULL,            #  Request body
  request_request_param_get    MEDIUMTEXT,                     #  Request GET parameters
  request_request_param_post   MEDIUMTEXT,                     #  Request POST parameters
  request_request_param_files  MEDIUMTEXT,                     #  Request uploaded files
  request_request_cookie       MEDIUMTEXT,                     #  Request cookies

  request_response_code        INT NOT NULL,                   #  Response HTTP code
  request_response_header      MEDIUMTEXT NOT NULL,            #  Response headers
  request_response_body        MEDIUMTEXT NOT NULL,            #  Response body

  PRIMARY KEY (request_uuid),
  INDEX (request_type),
  INDEX (request_tstamp,request_type)
)
ENGINE=INNODB
DEFAULT CHARSET=utf8mb4
DEFAULT COLLATE=utf8mb4_estonian_ci;


# --------------------------------------------------------------------------- #
