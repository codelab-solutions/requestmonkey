<?php


	/**
	 *
	 *   Request Monkey
	 *   --------------
	 *   Controller: log requests
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\RequestMonkey\Controller;

	use Codelab\FlaskPHP\Controller\ControllerInterface;


	class LogRequestController extends ControllerInterface
	{


		/**
		 *   Init controller
		 */

		function initController()
		{
			$this->setLoginRequired(false);
		}


		/**
		 *   Init actions
		 */

		function initActions()
		{
			$a=$this->addAction('logrequest');
			$a->setClass('Codelab\RequestMonkey\Action\LogRequestAction');
		}


	}


?>