<?php


	/**
	 *
	 *   Request Monkey
	 *   --------------
	 *   Action: log request action
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\RequestMonkey\Action;

	use Codelab\FlaskPHP\Action\ActionInterface;
	use Codelab\FlaskPHP\Exception\FatalException;
	use Codelab\FlaskPHP\Mailer\Mailer;
	use Codelab\FlaskPHP\Response\RawResponse;
	use Codelab\FlaskPHP\Response\ResponseInterface;
	use Codelab\FlaskPHP\Template\Template;
	use Codelab\FlaskPHP\Util;
	use Codelab\RequestMonkey\Model\Request;


	class LogRequestAction extends ActionInterface
	{


		public function runAction(): ResponseInterface
		{
			try
			{
				// Set defaults
				$logToDB=(Flask()->Config->get('requestmonkey.logtodb')?true:false);
				$logToMail=false;
				$logMailRecipient=$logMailSender=null;
				if (Flask()->Config->get('requestmonkey.sendmail'))
				{
					if (empty(Flask()->Config->get('requestmonkey.sendmail.recipient'))) throw new FatalException('Config value requestmonkey.sendmail.recipient is missing/empty.');
					$logMailRecipient=str_array(Flask()->Config->get('requestmonkey.sendmail.recipient'));
					foreach ($logMailRecipient as $r)
					{
						if (!Util::isValidEmail($r)) throw new FatalException('Recipient '.$r.' is not a valid e-mail address.');
					}
					if (empty(Flask()->Config->get('requestmonkey.sendmail.sender'))) throw new FatalException('Config value requestmonkey.sendmail.sender is missing/empty.');
					$logMailSender=Flask()->Config->get('requestmonkey.sendmail.sender');
					if (is_array($logMailSender))
					{
						if (!Util::isValidEmail($logMailSender[0])) throw new FatalException('Sender '.$logMailSender[0].' is not a valid e-mail address.');
					}
					else
					{
						if (!Util::isValidEmail($logMailSender)) throw new FatalException('Sender '.$logMailSender.' is not a valid e-mail address.');
					}
					$logMailSubject=oneof(Flask()->Config->get('requestmail.sendmail.subject'),'A logged request from the Request Monkey');
					$logToMail=true;
				}

				// If there's a config file, then read it
				$Config=null;
				if (Flask()->Config->get('requestmonkey.config'))
				{
					$configFileName=Flask()->Config->get('requestmonkey.config');
					if (!file_exists($configFileName)) throw new FatalException('Config file '.$configFileName.' does not exist.');
					if (!is_readable($configFileName)) throw new FatalException('Config file '.$configFileName.' is not readable.');
					$Config=json_decode(file_get_contents($configFileName));
					if ($Config===null) throw new FatalException('Unable to parse configuration: '.json_last_error_msg());
				}

				// Request type default
				$requestType=null;

				// Request URI
				$requestURI=$_SERVER['REQUEST_URI'];

				// Request method
				$requestMethod=Flask()->Request->requestMethod;

				// Request time
				$requestTimeStamp=date('Y-m-d H:i:s');

				// Remote IP/host
				$remoteIP=Flask()->Request->remoteIP();
				$remoteHost=Flask()->Request->remoteHost();

				// Request header
				$requestHeader=json_encode(
					Flask()->Request->requestHeader,
					JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
				);

				// Request body
				$requestBody=strval(file_get_contents('php://input'));

				// GET parameters
				if (sizeof($_GET))
				{
					$requestParamGET=json_encode(
						$_GET,
						JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
					);
				}
				else
				{
					$requestParamGET=null;
				}

				// POST parameters
				if (sizeof($_POST))
				{
					$requestParamPOST=json_encode(
						$_POST,
						JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
					);
				}
				else
				{
					$requestParamPOST=null;
				}

				// Files
				if (sizeof($_FILES))
				{
					$requestParamFiles=json_encode(
						$_FILES,
						JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
					);
				}
				else
				{
					$requestParamFiles=null;
				}

				// Cookies
				if (sizeof($_COOKIE))
				{
					$requestCookie=json_encode(
						$_COOKIE,
						JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
					);
				}
				else
				{
					$requestCookie=null;
				}

				// Default response
				$responseCode=200;
				$responseHeader=[
					'Content-Type' => 'text/plain'
				];
				$responseBody='OK!';

				// See if there's a match in config
				if ($Config!==null && is_array($Config->request))
				{
					foreach ($Config->request as $configRequestItem)
					{
						// Match request method
						if (!empty($configRequestItem->request_method) && $configRequestItem->request_method!=$requestMethod) continue;

						// Match URL
						if (!empty($configRequestItem->request_uri) && !preg_match('/'.$configRequestItem->request_uri.'/',$requestURI)) continue;

						// Log to DB?
						$logToDB=false;
						if ($configRequestItem->logtodb) $logToDB=true;

						// Log to mail
						$logToMail=false;
						$logMailRecipient=$logMailSender=null;
						if ($configRequestItem->sendmail)
						{
							if (empty($configRequestItem->sendmail->recipient)) throw new FatalException('Config value sendmail.recipient is missing/empty for matching configuration entry.');
							$logMailRecipient=str_array($configRequestItem->sendmail->recipient);
							foreach ($logMailRecipient as $r)
							{
								if (!Util::isValidEmail($r)) throw new FatalException('Recipient '.$r.' is not a valid e-mail address in matching configuration entry.');
							}
							if (empty($configRequestItem->sendmail->sender)) throw new FatalException('Config value requestmonkey.sendmail.sender is missing/empty in matching configuration entry.');
							$logMailSender=$configRequestItem->sendmail->sender;
							if (is_array($logMailSender))
							{
								if (!Util::isValidEmail($logMailSender[0])) throw new FatalException('Sender '.$logMailSender[0].' is not a valid e-mail address in matching configuration entry.');
							}
							else
							{
								if (!Util::isValidEmail($logMailSender)) throw new FatalException('Sender '.$logMailSender.' is not a valid e-mail address in matching configuration entry.');
							}
							$logMailSubject=oneof($configRequestItem->sendmail->subject,'A logged request from the Request Monkey');
							$logToMail=true;
						}

						// Response code
						if (!empty($configRequestItem->response_code))
						{
							$responseCode=$configRequestItem->response_code;
						}

						// Response headers
						if (!empty($configRequestItem->response_header))
						{
							$responseHeader=get_object_vars($configRequestItem->response_header);
						}

						// Response body
						if (!empty($configRequestItem->response_body))
						{
							$responseBody=strval($configRequestItem->response_body);
						}

						// Found our match - good enough.
						break;
					}
				}

				// Log to DB
				if ($logToDB)
				{
					$Request=new Request();
					$Request->request_type=$requestType;
					$Request->request_tstamp=$requestTimeStamp;
					$Request->request_uri=$requestURI;
					$Request->request_method=$requestMethod;
					$Request->request_remote_ip=$remoteIP;
					$Request->request_remote_host=$remoteHost;
					$Request->request_request_header=$requestHeader;
					$Request->request_request_body=$requestBody;
					$Request->request_request_param_get=$requestParamGET;
					$Request->request_request_param_post=$requestParamPOST;
					$Request->request_request_param_files=$requestParamFiles;
					$Request->request_request_cookie=$requestCookie;
					$Request->request_response_code=$responseCode;
					$Request->request_response_header=json_encode(
						$responseHeader,
						JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
					);
					$Request->request_response_body=strval($responseBody);
					$Request->save();
				}

				// Log to mail
				if ($logToMail)
				{
					// Build mail content
					$mailContent='<p>The following request was catched by Request Monkey:</p>';
					$mailContent.='<p><ul>';
					$mailContent.='<li><b>Date/time:</b> '.$requestTimeStamp.'</li>';
					$mailContent.='<li><b>URI:</b> '.$requestURI.'</li>';
					$mailContent.='<li><b>Method:</b> '.$requestMethod.'</li>';
					$mailContent.='<li><b>Remote IP:</b> '.$remoteIP.'</li>';
					$mailContent.='<li><b>Remote hostname:</b> '.$remoteHost.'</li>';
					$mailContent.='</ul></p>';
					if (!empty($requestHeader))
					{
						$mailContent.='<h3>Request headers:</h3>';
						$mailContent.='<p><pre>'.$requestHeader.'</pre></p>';
					}
					if (!empty($requestParamGET))
					{
						$mailContent.='<h3>GET variables:</h3>';
						$mailContent.='<p><pre>'.$requestParamGET.'</pre></p>';
					}
					if (!empty($requestParamPOST))
					{
						$mailContent.='<h3>POST variables:</h3>';
						$mailContent.='<p><pre>'.$requestParamPOST.'</pre></p>';
					}
					if (!empty($requestParamFiles))
					{
						$mailContent.='<h3>Submitted files:</h3>';
						$mailContent.='<p><pre>'.$requestParamFiles.'</pre></p>';
					}
					if (!empty($requestBody))
					{
						$mailContent.='<h3>Raw request body:</h3>';
						$mailContent.='<p><pre>'.$requestBody.'</pre></p>';
					}
					if (!empty($requestCookie))
					{
						$mailContent.='<h3>Cookies:</h3>';
						$mailContent.='<p><pre>'.$requestCookie.'</pre></p>';
					}
					$mailContent.='<h3>Response</h3>';
					$mailContent.='<p><ul>';
					$mailContent.='<li><b>Response HTTP code:</b> '.$responseCode.'</li>';
					$mailContent.='</ul></p>';
					if (!empty($responseHeader))
					{
						$mailContent.='<h3>Response headers:</h3>';
						$mailContent.='<p><pre>'.json_encode($responseHeader,JSON_HEX_QUOT|JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT).'</pre></p>';
					}
					if (!empty($responseBody))
					{
						$mailContent.='<h3>Response body:</h3>';
						$mailContent.='<p><pre>'.$responseBody.'</pre></p>';
					}

					// Build subject
					$mailSubject=$logMailSubject;
					if (strpos($mailSubject,'{')!==false)
					{
						$mailVars=[];
						$mailVars['request_type']=strval($requestType);
						$mailVars['request_tstamp']=$requestTimeStamp;
						$mailVars['request_uri']=$requestURI;
						$mailVars['request_method']=$requestMethod;
						$mailVars['remote_ip']=$remoteIP;
						$mailVars['remote_host']=$remoteHost;
						$mailVars['response_code']=intval($responseCode);
						$mailSubject=Template::parseVariables($mailSubject,$mailVars);
					}

					// Send mail
					$Mailer=new Mailer();
					if (is_array($logMailSender))
					{
						$Mailer->setFrom($logMailSender[0],$logMailSender[1]);
					}
					else
					{
						$Mailer->setFrom($logMailSender);
					}
					$Mailer->setSubject($mailSubject);
					$Mailer->MsgHTML($mailContent);
					foreach ($logMailRecipient as $recipient)
					{
						$Mailer->AddAddress($recipient);
						$Mailer->send();
					}
				}

				// Send response
				$Response=new RawResponse();
				$Response->setContentType(oneof($responseHeader['Content-Type'],'text/plain'));
				foreach ($responseHeader as $h => $v)
				{
					$Response->setHeader($h,$v);
				}
				$Response->setStatus($responseCode);
				$Response->setContent($responseBody);
				return $Response;
			}
			catch (\Exception $e)
			{
				$Response=new RawResponse();
				$Response->setContentType('text/plain');
				$Response->setStatus(500);
				$Response->setContent('ERROR: '.$e->getMessage());
				return $Response;
			}
		}


	}


?>