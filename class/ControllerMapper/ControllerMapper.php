<?php


	/**
	 *
	 *   Request Monkey
	 *   --------------
	 *   Controller mapper class: map everything to logrequest controller
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\RequestMonkey\ControllerMapper;

	use Codelab\FlaskPHP\Controller\ControllerInterface;
	use Codelab\FlaskPHP\Request\RequestControllerMapperInterface;


	class ControllerMapper extends RequestControllerMapperInterface
	{


		/**
		 *    Run the controller mapper
		 */

		public function runControllerMapper( string $uriElement, array &$uriArray ): ?ControllerInterface
		{
			$controllerFilename=Flask()->getAppPath().'/controller/logrequest.php';
			return $this->getControllerObject($controllerFilename);
		}


	}


?>