<?php


	/**
	 *
	 *   Request Monkey
	 *   --------------
	 *   Controller mapper class: map everything to logrequest controller
	 *
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\RequestMonkey\Model;

	use Codelab\FlaskPHP\Model\ModelInterface;

	class Request extends ModelInterface
	{


		/**
		 *
		 *   Init model
		 *   ----------
		 *
		 */

		public function initModel()
		{
			// Main parameters
			$this->setParam('table','requestmonkey_request');
			$this->setParam('idfield','request_uuid');
			$this->setParam('idtype','uuid');
			$this->setParam('modfields',false);

			// Logging: don't need to log the log :)
			$this->setParam('log',false);
		}


	}


?>