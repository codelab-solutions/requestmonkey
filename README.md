# Request Monkey - a tool that will gladly accept and log any HTTP request thrown at it

### Pre-requisites

* A Linux/Mac machine with Apache running on it
* PHP version 7.2 or newer
* MySQL/MariaDB 5.5 or newer, if you want to log into a database
* Access to a SMTP server if you want mailto-logging

### Installation

* Clone this repository
* Run `composer install`
* Update `config/config.php` with your general configuration
* Set up an Apache virtualhost with `$repositorypath/public` as the DocumentRoot
* If you plan on logging into the DB, follow the "Setting up the database" chapter

### Setting up the database

If you plan to only use mailto-logging, then you can skip this step.

* Create a MySQL/MariaDB database
* Set up appropriate permissions
* Load the `vendor/codelab/flaskphp/dbstruct/dbstruct.flask.master.sql` file into your database
* Load the `dbstruct/dbstruct.requestmonkey.master.sql` file into your database

### Configuration

If you want to log into the database:

* Update the database access credentials in the `db` section
* Set the `logtodb` parameter in the `requestmonkey` section to `true`

If you want to use mailto-logging:

* Update the `smtp` section with your SMTP server hostname and port. If your server uses authentication, you can provide the `username` and `password` configuration directives.
* Set the `recipient` parameter in the `requestmonkey.sendmail` section to your e-mail address. Multiple e-mails can be used using a comma or providing an array.
* Set the `sender` parameter in the `requestmonkey.sendmail` section to the e-mail address that will be used as the sender. Make sure the system is allowed to send e-mail as that address (SPF and friends).
* The `sender` parameter can either be a string (e-mail address) or a 2-element array, where the first element is the e-mail address and the second is the display name.
* The optional `subject` parameter can contain the message subject.

### Using variables in the mail subject

The mail subject parameter can contain the following variables in the `{{ variable }}` format:

* request_type
* request_tstamp
* request_uri
* request_method
* remote_ip
* remote_host
* response_code

### Configuration file format

Here's a sample configuration file:
```json
{
  "request": [
    {
      "request_uri": "^\\/foo\\/bar.*",
      "request_method": "GET",
      "response_code": 500,
      "response_header": {
        "Content-Type": "application/json",
        "Pragma": "no-cache"
      },
      "response_body": "{ status: \"ERROR\", error: \"Whoopsie-daisy, something went wrong here.\" }",
      "logtodb": false,
      "sendmail": {
        "recipient": "sample@recipie.nt",
        "sender": "sample@send.er",
        "subject": "A {{ request_method }} request to {{ request_uri }} has been made!"
      }
    }
  ]
    
}
```

In short:

* The JSON should contain a `request` array, that consists of objects.
* The `request_uri` and `request_method` variables are both optional - if missing, then all requests match.
* The `request_uri` variable must contain a valid regular expression.
* The first matching entry is used, so if you are using overlapping entries, put them in the descending order of detail.


### Using this with Nginx instead of Apache

* If you get that working, let us know and we'll update the documentation. 😊 
