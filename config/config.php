<?php


	//
	//  Configuration
	//

	return [


		// Main app settings
		'app' => [
			'id' => 'requestmonkey'
		],
	
	
		// Database
		'db' => [
			'type' => 'MySQL',
			'mysql' => [
				'hostname' => 'databasehost',
				'username' => 'databaseuser',
				'password' => 'databasepassword',
				'database' => 'databasename'
			]
		],
	
	
		// Locale
		'locale' => [
			'lang' => ['et'],
			'dateformat' => 'dd.mm.yyyy',
			'timezone' => 'Europe/Tallinn'
		],


		// Request
		'request' => [
			'controllermapper' => [
				[
					'mapper' => new \Codelab\RequestMonkey\ControllerMapper\ControllerMapper(),
					'priority' => -1
				]
			]
		],

		
		// SMTP
		'smtp' => [
			'host' => 'your.smtp.server',
			'port' => 587
		],
		
		
		// RequestMonkey specific settings
		'requestmonkey' => [
			'logtodb' => true,
			'sendmail' => [
				'recipient' => 'your.email@addre.ss',
				'sender' => 'sender.email@addre.ss'
			],
			'config' => '/path/to/requestmonkey-config.json'
		]

	
	];


?>